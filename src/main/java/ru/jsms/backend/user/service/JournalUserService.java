package ru.jsms.backend.user.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.jsms.backend.admin.entity.Journal;
import ru.jsms.backend.admin.repository.JournalRepository;
import ru.jsms.backend.common.dto.PageDto;
import ru.jsms.backend.common.dto.PageParam;
import ru.jsms.backend.user.dto.response.JournalFullResponse;
import ru.jsms.backend.user.dto.response.JournalResponse;
import ru.jsms.backend.user.dto.response.SectionResponse;

import java.util.stream.Collectors;

import static ru.jsms.backend.user.enums.ArticleExceptionCode.ACCESS_DENIED;
import static ru.jsms.backend.user.enums.ArticleExceptionCode.JOURNAL_NOT_FOUND;

@Service
@RequiredArgsConstructor
public class JournalUserService {

    private final JournalRepository journalRepository;

    public PageDto<JournalResponse> getAllJournals(PageParam pageParam) {
        return new PageDto<>(journalRepository.findPublished(pageParam.toPageable()).map(this::convertToResponse));
    }

    public JournalFullResponse getJournal(Long id) {
        Journal journal = journalRepository.findById(id).orElseThrow(JOURNAL_NOT_FOUND.getException());
        if (!journal.getPublished()) {
            throw ACCESS_DENIED.getException();
        }
        return convertToFullResponse(journal);
    }

    private JournalResponse convertToResponse(Journal journal) {
        return JournalResponse.builder()
                .id(journal.getId())
                .volume(journal.getVolume())
                .number(journal.getNumber())
                .issn(journal.getIssn())
                .build();
    }

    private JournalFullResponse convertToFullResponse(Journal journal) {
        return JournalFullResponse.builder()
                .id(journal.getId())
                .volume(journal.getVolume())
                .number(journal.getNumber())
                .issn(journal.getIssn())
                .fileId(journal.getFileId())
                .publishingDate(journal.getUpdated().toLocalDate())
                .sections(journal.getSections().stream().map(SectionResponse::new).collect(Collectors.toSet()))
                .build();
    }
}
