package ru.jsms.backend.user.service;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import ru.jsms.backend.common.dto.PageDto;
import ru.jsms.backend.common.dto.PageParam;
import ru.jsms.backend.user.dto.request.CreateAuthorRequest;
import ru.jsms.backend.user.dto.response.AuthorFullResponse;
import ru.jsms.backend.user.dto.response.AuthorResponse;
import ru.jsms.backend.user.dto.response.OfferArticleResponse;
import ru.jsms.backend.user.entity.Author;
import ru.jsms.backend.user.repository.AuthorRepository;

import java.util.stream.Collectors;

import static org.apache.commons.lang3.StringUtils.isBlank;
import static ru.jsms.backend.user.enums.ArticleExceptionCode.AUTHOR_ALREADY_EXISTS;
import static ru.jsms.backend.user.enums.ArticleExceptionCode.AUTHOR_DELETE_DENIED;
import static ru.jsms.backend.user.enums.ArticleExceptionCode.AUTHOR_NOT_FOUND;

@RequiredArgsConstructor
@Service
public class AuthorService {

    private final AuthorRepository authorRepository;

    public PageDto<AuthorResponse> getAuthorsByFullname(PageParam pageParam, String fullnameSubstring) {
        Page<Author> authorPage;
        if (isBlank(fullnameSubstring)) {
            authorPage = authorRepository.findAll(pageParam.toPageable());
        } else {
            authorPage = authorRepository.findByFullnameLike(fullnameSubstring, pageParam.toPageable());
        }
        return new PageDto<>(authorPage.map(this::convertToResponse));
    }

    public AuthorFullResponse getAuthor(Long authorId) {
        return convertToFullResponse(authorRepository.findById(authorId).orElseThrow(AUTHOR_NOT_FOUND.getException()));
    }

    public AuthorFullResponse createAuthor(CreateAuthorRequest request) {
        if (authorRepository.findByEmail(request.getEmail()).isPresent()) {
            throw AUTHOR_ALREADY_EXISTS.getException();
        }
        Author author = Author.builder()
                .firstName(request.getFirstName())
                .secondName(request.getSecondName())
                .patronymic(request.getPatronymic())
                .email(request.getEmail())
                .academicDegree(request.getAcademicDegree())
                .orcid(request.getOrcid())
                .post(request.getPost())
                .organization(request.getOrganization())
                .build();
        return convertToFullResponse(authorRepository.save(author));
    }

    public void deleteAuthor(Long authorId) {
        Author author = authorRepository.findById(authorId).orElse(null);
        if (author == null) {
            return;
        }
        if (!author.getArticles().isEmpty()) {
            throw AUTHOR_DELETE_DENIED.getException();
        }
        authorRepository.deleteById(authorId);
    }

    private AuthorResponse convertToResponse(Author author) {
        return new AuthorResponse(author);
    }

    private AuthorFullResponse convertToFullResponse(Author author) {
        return AuthorFullResponse.builder()
                .id(author.getId())
                .email(author.getEmail())
                .firstName(author.getFirstName())
                .secondName(author.getSecondName())
                .patronymic(author.getPatronymic())
                .articles(author.getArticles().stream().map(OfferArticleResponse::new).collect(Collectors.toList()))
                .academicDegree(author.getAcademicDegree())
                .post(author.getPost())
                .organization(author.getOrganization())
                .orcid(author.getOrcid())
                .build();
    }
}
