package ru.jsms.backend.user.dto.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import ru.jsms.backend.user.enums.OfferArticleStatus;

import java.time.LocalDateTime;

@Data
@Builder
@AllArgsConstructor
public class OfferArticleStatusHistoryDto {
    private OfferArticleStatus status;
    private LocalDateTime updated;
}
