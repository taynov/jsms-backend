package ru.jsms.backend.user.dto.response;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@Data
@Builder
public class OfferArticleFullResponse {
    private Long id;
    private String name;
    private String status;
    private String udk;
    private String annotation;
    private LocalDateTime created;
    private List<AuthorResponse> authors;
    private List<OfferArticleStatusHistoryDto> statusHistory;
}
