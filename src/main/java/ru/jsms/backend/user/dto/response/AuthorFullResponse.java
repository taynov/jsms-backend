package ru.jsms.backend.user.dto.response;

import lombok.Builder;
import lombok.Data;
import ru.jsms.backend.user.entity.embeddable.Organization;
import ru.jsms.backend.user.enums.AcademicDegree;

import java.util.List;

@Data
@Builder
public class AuthorFullResponse {
    private Long id;
    private String firstName;
    private String secondName;
    private String patronymic;
    private String email;
    private AcademicDegree academicDegree;
    private String post;
    private Organization organization;
    private String orcid;
    private List<OfferArticleResponse> articles;
}
