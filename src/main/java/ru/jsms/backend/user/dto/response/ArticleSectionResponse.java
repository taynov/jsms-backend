package ru.jsms.backend.user.dto.response;

import lombok.Data;
import ru.jsms.backend.admin.entity.Article;

import java.util.Set;
import java.util.stream.Collectors;

@Data
public class ArticleSectionResponse {
    private String name;
    private Set<AuthorResponse> authors;

    public ArticleSectionResponse(Article article) {
        this.name = article.getOfferArticle().getName();
        this.authors = article.getOfferArticle().getAuthors().stream()
                .map(AuthorResponse::new).collect(Collectors.toSet());
    }
}
