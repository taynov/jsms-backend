package ru.jsms.backend.user.dto.request;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.Set;

@Data
public class CreateOfferArticleRequest {
    @NotBlank
    private String name;

    @Size(min = 1, max = 20)
    private String udk;

    private String annotation;

    private Set<Long> authorIds;
}
