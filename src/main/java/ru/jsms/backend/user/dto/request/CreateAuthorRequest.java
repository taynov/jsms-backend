package ru.jsms.backend.user.dto.request;

import lombok.Data;
import ru.jsms.backend.user.entity.embeddable.Organization;
import ru.jsms.backend.user.enums.AcademicDegree;

import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class CreateAuthorRequest {

    @NotBlank
    private String firstName;

    @NotBlank
    private String secondName;

    private String patronymic;

    @NotNull
    @Email
    private String email;

    @NotNull
    private AcademicDegree academicDegree;

    private String post;

    @Valid
    @NotNull
    private Organization organization;

    @Size(min = 19, max = 19)
    @NotBlank
    private String orcid;

}
