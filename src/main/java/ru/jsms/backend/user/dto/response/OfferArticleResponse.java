package ru.jsms.backend.user.dto.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import ru.jsms.backend.user.entity.OfferArticle;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@Builder
public class OfferArticleResponse {
    private Long id;
    private String name;
    private String status;
    private String udk;
    private LocalDateTime created;

    public OfferArticleResponse(OfferArticle offerArticle) {
        this.id = offerArticle.getId();
        this.name = offerArticle.getName();
        this.status = String.valueOf(offerArticle.getStatus());
        this.udk = offerArticle.getUdk();
        this.created = offerArticle.getCreated();
    }
}
