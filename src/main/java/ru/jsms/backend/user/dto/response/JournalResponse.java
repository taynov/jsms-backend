package ru.jsms.backend.user.dto.response;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class JournalResponse {
    private Long id;
    private Integer volume;
    private Integer number;
    private String issn;
}
