package ru.jsms.backend.user.dto.response;

import lombok.Getter;
import org.springframework.data.domain.Page;
import ru.jsms.backend.common.dto.PageDto;

@Getter
public class OfferArticleVersionsPageResponse extends PageDto<OfferArticleVersionResponse> {

    private final boolean showDraftVersionButton;

    public OfferArticleVersionsPageResponse(Page<OfferArticleVersionResponse> page, boolean showDraftVersionButton) {
        super(page);
        this.showDraftVersionButton = showDraftVersionButton;
    }
}
