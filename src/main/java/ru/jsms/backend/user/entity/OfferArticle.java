package ru.jsms.backend.user.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import ru.jsms.backend.common.entity.BaseOwneredEntity;
import ru.jsms.backend.user.enums.OfferArticleStatus;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import static org.apache.commons.lang3.StringUtils.isBlank;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@Entity
@Table(name = "offer_article")
public class OfferArticle extends BaseOwneredEntity<Long> {

    private String name;

    @Enumerated(EnumType.STRING)
    @Builder.Default
    private OfferArticleStatus status = OfferArticleStatus.DRAFT;

    @Size(min = 1, max = 20)
    private String udk;

    private String annotation;

    @OneToMany(mappedBy = "offerArticle", cascade = CascadeType.ALL)
    @Builder.Default
    private Set<OfferArticleVersion> versions = new HashSet<>();

    public Set<OfferArticleVersion> getVersions() {
        return versions.stream().filter(s -> !s.isDeleted()).collect(Collectors.toSet());
    }

    @ManyToMany
    @JoinTable(
            name = "article_authors",
            joinColumns = {@JoinColumn(name = "article_id")},
            inverseJoinColumns = {@JoinColumn(name = "author_id")}
    )
    @Builder.Default
    private Set<Author> authors = new HashSet<>();

    public Set<Author> getAuthors() {
        return authors.stream().filter(s -> !s.isDeleted()).collect(Collectors.toSet());
    }

    public boolean isComplete() {
        return !isBlank(name) && !authors.isEmpty() && !isBlank(udk) && !isBlank(annotation);
    }
}
