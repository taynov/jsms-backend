package ru.jsms.backend.user.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import ru.jsms.backend.common.entity.BaseEntity;
import ru.jsms.backend.user.entity.embeddable.Organization;
import ru.jsms.backend.user.enums.AcademicDegree;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToMany;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@Entity
public class Author extends BaseEntity<Long> {

    @NotBlank
    private String firstName;

    @NotBlank
    private String secondName;

    private String patronymic;

    @NotNull
    @Email
    private String email;

    @NotNull
    @Enumerated(EnumType.STRING)
    private AcademicDegree academicDegree;

    private String post;

    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "name", column = @Column(name = "organization_name")),
            @AttributeOverride(name = "address", column = @Column(name = "organization_address"))
    })
    @NotNull
    private Organization organization;

    @Size(min = 19, max = 19)
    @NotBlank
    private String orcid;

    @ManyToMany(mappedBy = "authors")
    @Builder.Default
    private Set<OfferArticle> articles = new HashSet<>();

    public Set<OfferArticle> getArticles() {
        return articles.stream().filter(s -> !s.isDeleted()).collect(Collectors.toSet());
    }
}
