package ru.jsms.backend.user.entity.embeddable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;
import javax.validation.constraints.NotBlank;

@Data
@Builder
@Embeddable
@NoArgsConstructor
@AllArgsConstructor
public class Organization {

    @NotBlank
    private String name;

    @NotBlank
    private String address;
}




