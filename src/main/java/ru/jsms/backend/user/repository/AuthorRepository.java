package ru.jsms.backend.user.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.jsms.backend.user.entity.Author;
import ru.jsms.backend.common.repository.BaseRepository;

import java.util.Optional;

@Repository
public interface AuthorRepository extends BaseRepository<Author, Long> {
    @Transactional(readOnly = true)
    @Query("select a from Author a where a.email = ?1 and a.deleted = false")
    Optional<Author> findByEmail(String email);

    @Transactional(readOnly = true)
    @Query(value = """
            select author.* from author, to_tsquery(text(plainto_tsquery(?1)) || ':*') query,
                             LATERAL (
                               select setweight(to_tsvector('simple', second_name), 'A') ||
                                      setweight(to_tsvector('simple', first_name), 'B') ||
                                      setweight(to_tsvector('simple', coalesce(patronymic, '')), 'C')
                             ) ts (textsearch)
            where textsearch @@ query
            and deleted = false
            order by ts_rank(textsearch, query) desc
            """, nativeQuery = true)
     Page<Author> findByFullnameLike(String fullnameSubstring, Pageable pageable);
}
