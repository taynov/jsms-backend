package ru.jsms.backend.user.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.jsms.backend.common.repository.BaseOwneredRepository;
import ru.jsms.backend.user.entity.OfferArticle;
import ru.jsms.backend.user.enums.OfferArticleStatus;

@Repository
public interface OfferArticleRepository extends BaseOwneredRepository<OfferArticle, Long> {
    @Query("select o from OfferArticle o where o.ownerId=:userId and o.status in :statuses and o.deleted = false")
    Page<OfferArticle> findByOwnerIdAndStatuses(Long userId, OfferArticleStatus[] statuses, Pageable pageable);
}
