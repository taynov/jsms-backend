package ru.jsms.backend.user.controller;

import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.jsms.backend.common.dto.PageDto;
import ru.jsms.backend.common.dto.PageParam;
import ru.jsms.backend.user.dto.request.CreateOfferArticleRequest;
import ru.jsms.backend.user.dto.request.EditOfferArticleRequest;
import ru.jsms.backend.user.dto.response.OfferArticleFullResponse;
import ru.jsms.backend.user.dto.response.OfferArticleResponse;
import ru.jsms.backend.user.enums.OfferArticleStatus;
import ru.jsms.backend.user.service.OfferArticleService;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/v1/offerArticles")
@RequiredArgsConstructor
@PreAuthorize("hasAuthority('USER')")
public class OfferArticleController {

    private final OfferArticleService offerArticleService;

    @Operation(summary = "Get a page of article offers",
            description = "Returns a page of article offers sorted by date of creation")
    @GetMapping
    public ResponseEntity<PageDto<OfferArticleResponse>> getOfferArticles(
            PageParam pageParam,
            @RequestParam(name = "status", required = false) OfferArticleStatus[] statuses) {
        return ResponseEntity.ok(offerArticleService.getOfferArticles(pageParam, statuses));
    }

    @GetMapping("/{id}")
    public ResponseEntity<OfferArticleFullResponse> getOfferArticle(@PathVariable Long id) {
        return ResponseEntity.ok(offerArticleService.getOfferArticle(id));
    }

    @PostMapping
    public ResponseEntity<OfferArticleFullResponse> createOfferArticle(
            @Valid @RequestBody CreateOfferArticleRequest request) {
        return ResponseEntity.ok(offerArticleService.createOfferArticle(request));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteOfferArticle(@PathVariable Long id) {
        offerArticleService.deleteOfferArticle(id);
        return ResponseEntity.ok().build();
    }

    @PutMapping("/{id}")
    public ResponseEntity<OfferArticleFullResponse> editOfferArticle(@PathVariable Long id,
                                                                 @Valid @RequestBody EditOfferArticleRequest request) {
        return ResponseEntity.ok(offerArticleService.editOfferArticle(id, request));
    }
}
