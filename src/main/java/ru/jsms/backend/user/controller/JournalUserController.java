package ru.jsms.backend.user.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.jsms.backend.common.dto.PageDto;
import ru.jsms.backend.common.dto.PageParam;
import ru.jsms.backend.user.dto.response.JournalFullResponse;
import ru.jsms.backend.user.dto.response.JournalResponse;
import ru.jsms.backend.user.service.JournalUserService;

@RestController
@RequestMapping("/api/v1/journals")
@RequiredArgsConstructor
public class JournalUserController {

    private final JournalUserService journalService;

    @GetMapping
    public ResponseEntity<PageDto<JournalResponse>> getAllJournals(PageParam pageParam) {
        return ResponseEntity.ok(journalService.getAllJournals(pageParam));
    }

    @GetMapping("/{id}")
    public ResponseEntity<JournalFullResponse> getJournal(@PathVariable Long id) {
        return ResponseEntity.ok(journalService.getJournal(id));
    }

}
