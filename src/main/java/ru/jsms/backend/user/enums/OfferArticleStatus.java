package ru.jsms.backend.user.enums;

public enum OfferArticleStatus {
    DRAFT, CONSIDERATION, REVISION, REJECTED, ACCEPTED, PUBLISHED
}
