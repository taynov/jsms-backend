package ru.jsms.backend.user.enums;

public enum AcademicDegree {
    NONE, BACHELOR, MASTER, SPECIALIST, CANDIDATE, DOCTOR
}
