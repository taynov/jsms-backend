package ru.jsms.backend.common.dto;

import io.swagger.v3.oas.annotations.Parameter;
import lombok.Data;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;

@Data
public class PageParam {
    @Parameter(name = "page", description = "Page number, numbering starts from 0. Default is 0", example = "0")
    private int page = 0;
    @Parameter(name = "size", description = "Number of items per page. Default is 10", example = "10")
    private int size = 10;
    @Parameter(name = "orderByDate", description = "Direction of sort, can be DESC or ASC. Default is DESC", example = "ASC")
    private Direction orderByDate = Direction.DESC;

    public Pageable toPageable() {
        return PageRequest.of(page, size, Sort.by(orderByDate, "created"));
    }
}
