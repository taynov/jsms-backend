package ru.jsms.backend.common.dto;

import lombok.Data;
import org.slf4j.MDC;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.RequestScope;
import ru.jsms.backend.profile.entity.User;
import ru.jsms.backend.profile.repository.UserRepository;

import javax.servlet.http.HttpServletRequest;

@Component
@RequestScope
@Data
public class HeadersDto {

    private final User user;
    private final Long userId;
    private final boolean isAdmin;
    private final String ip;
    private final String xRequestId;
    private final Role currentRole;

    public HeadersDto(HttpServletRequest request, UserRepository repository) {
        ip = request.getRemoteAddr();
        xRequestId = MDC.get("x-request-id");
        var authInfo = (JwtAuthentication) request.getUserPrincipal();
        if (authInfo == null) {
            userId = null;
            isAdmin = false;
            user = null;
            currentRole = null;
            return;
        }
        userId = authInfo.getUserId();
        isAdmin = authInfo.getAuthorities().contains(Role.ADMIN);
        currentRole = (Role) authInfo.getAuthorities().stream().findFirst().orElse(null);
        user = repository.findById(userId).orElse(null);
    }
}
