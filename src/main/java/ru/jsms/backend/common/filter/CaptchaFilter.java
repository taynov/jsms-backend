package ru.jsms.backend.common.filter;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;
import org.springframework.util.PathMatcher;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.servlet.HandlerExceptionResolver;
import ru.jsms.backend.common.config.properties.CaptchaProperties;
import ru.jsms.backend.common.service.CaptchaService;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
@ConditionalOnProperty(name = "captcha.enabled", havingValue = "true")
@RequiredArgsConstructor
public class CaptchaFilter extends OncePerRequestFilter {

    private final CaptchaProperties captchaProperties;
    private final CaptchaService captchaService;
    private final HandlerExceptionResolver handlerExceptionResolver;
    private final PathMatcher pathMatcher;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        String url = request.getServletPath();
        String method = request.getMethod();

        boolean needVerification = captchaProperties.getPaths().stream()
                .anyMatch(p -> p.getMethod().equals(method) && pathMatcher.match(p.getUrl(), url));
        if (!needVerification) {
            filterChain.doFilter(request, response);
            return;
        }
        try {
            captchaService.verify(request.getParameter(captchaProperties.getRequestParam()));
            filterChain.doFilter(request, response);
        } catch (Exception e) {
            handlerExceptionResolver.resolveException(request, response, null, e);
        }
    }
}
