package ru.jsms.backend.common.filter;

import org.slf4j.MDC;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;
import java.util.UUID;

@Component
@Order(Ordered.HIGHEST_PRECEDENCE)
public class RequestIdFilter extends OncePerRequestFilter {
    private static final String X_REQUEST_ID = "x-request-id";

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        String xRequestId = Optional.ofNullable(request.getHeader(X_REQUEST_ID))
                .orElse(UUID.randomUUID().toString());
        MDC.put(X_REQUEST_ID, xRequestId);
        response.addHeader(X_REQUEST_ID, xRequestId);
        filterChain.doFilter(request, response);
        MDC.clear();
    }
}
