package ru.jsms.backend.common.service;

import io.jsonwebtoken.Claims;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import ru.jsms.backend.common.dto.JwtAuthentication;
import ru.jsms.backend.common.dto.Role;

import static ru.jsms.backend.profile.enums.ClaimsCode.ROLE;
import static ru.jsms.backend.profile.enums.ClaimsCode.USER_ID;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class JwtUtils {

    public static JwtAuthentication generate(Claims claims) {
        final JwtAuthentication jwtInfoToken = new JwtAuthentication();
        jwtInfoToken.setRole(Role.valueOf(claims.get(ROLE.getName(), String.class)));
        jwtInfoToken.setUserId(claims.get(USER_ID.getName(), Long.class));
        return jwtInfoToken;
    }

}
