package ru.jsms.backend.common.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import ru.jsms.backend.common.config.properties.CaptchaProperties;
import ru.jsms.backend.common.dto.GoogleResponse;
import ru.jsms.backend.common.dto.HeadersDto;

import java.net.URI;

import static ru.jsms.backend.common.enums.CommonExceptionCode.CAPTCHA_INVALID;

@Service
@RequiredArgsConstructor
public class CaptchaService {

    private static final String SECRET_PARAM = "secret";
    private static final String TOKEN_PARAM = "response";
    private static final String IP_PARAM = "remoteip";

    private final CaptchaProperties captchaProperties;
    private final RestTemplate restTemplate;
    private final HeadersDto headersDto;

    public void verify(String captchaToken) {
        if (captchaToken == null) {
            throw CAPTCHA_INVALID.getException();
        }
        GoogleResponse googleResponse = restTemplate.getForObject(getVerificationUri(captchaToken), GoogleResponse.class);
        if (googleResponse != null && !googleResponse.isSuccess()) {
            throw CAPTCHA_INVALID.getException();
        }
    }

    private URI getVerificationUri(String captchaToken) {
        return UriComponentsBuilder.fromUri(URI.create(captchaProperties.getVerificationUrl()))
                .queryParam(SECRET_PARAM, captchaProperties.getSecret())
                .queryParam(TOKEN_PARAM, captchaToken)
                .queryParam(IP_PARAM, headersDto.getIp())
                .build().toUri();
    }
}
