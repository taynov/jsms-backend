package ru.jsms.backend.common.service;

import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Component;

import javax.mail.internet.MimeMessage;
import java.io.InputStream;

@Component
@ConditionalOnProperty(name = "emailSenderMock.enabled", havingValue = "true")
@Slf4j
public class MockJavaMailSender implements JavaMailSender {

    @NotNull
    @Override
    public MimeMessage createMimeMessage() {
        throw new UnsupportedOperationException();
    }

    @NotNull
    @Override
    public MimeMessage createMimeMessage(@NotNull InputStream contentStream) throws MailException {
        throw new UnsupportedOperationException();
    }

    @Override
    public void send(@NotNull MimeMessage mimeMessage) throws MailException {
        throw new UnsupportedOperationException();
    }

    @Override
    public void send(@NotNull MimeMessage... mimeMessages) throws MailException {
        throw new UnsupportedOperationException();
    }

    @Override
    public void send(@NotNull MimeMessagePreparator mimeMessagePreparator) throws MailException {
        throw new UnsupportedOperationException();
    }

    @Override
    public void send(@NotNull MimeMessagePreparator... mimeMessagePreparators) throws MailException {
        throw new UnsupportedOperationException();
    }

    @Override
    public void send(@NotNull SimpleMailMessage simpleMessage) throws MailException {
        log.info("MOCK: На почту {} отправлено письмо: {}", simpleMessage.getTo(), simpleMessage.getText());
    }

    @Override
    public void send(@NotNull SimpleMailMessage... simpleMessages) throws MailException {
        throw new UnsupportedOperationException();
    }
}
