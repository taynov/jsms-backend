package ru.jsms.backend.common.config;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;
import org.zalando.logbook.spring.LogbookClientHttpRequestInterceptor;

@Configuration
public class RestTemplateConfig {

    @Bean
    public RestTemplate restTemplate(LogbookClientHttpRequestInterceptor interceptor,
                                     RestTemplateBuilder builder) {
        return builder.additionalInterceptors(interceptor).build();
    }
}
