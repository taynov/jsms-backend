package ru.jsms.backend.common.config.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.List;

@Data
@ConfigurationProperties(prefix = "captcha")
public class CaptchaProperties {
    private Boolean enabled;
    private String secret;
    private String verificationUrl;
    private String requestParam;
    private List<Path> paths;

    @Data
    public static class Path {
        private String method;
        private String url;
    }
}
