package ru.jsms.backend.admin.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.jsms.backend.admin.dto.request.EditOfferArticleAnswerRequest;
import ru.jsms.backend.admin.dto.response.OfferArticleAnswerResponse;
import ru.jsms.backend.admin.entity.OfferArticleAnswer;
import ru.jsms.backend.admin.repository.OfferArticleAnswerRepository;
import ru.jsms.backend.files.service.FileService;
import ru.jsms.backend.profile.service.NotificationService;
import ru.jsms.backend.user.entity.OfferArticleVersion;
import ru.jsms.backend.user.service.OfferArticleVersionService;

import static org.apache.commons.lang3.StringUtils.isBlank;
import static ru.jsms.backend.admin.enums.AdminArticleExceptionCode.ANSWER_NOT_COMPLETE;
import static ru.jsms.backend.admin.enums.AdminArticleExceptionCode.ANSWER_NOT_FOUND;
import static ru.jsms.backend.common.utils.UuidUtils.parseUuid;
import static ru.jsms.backend.user.enums.ArticleExceptionCode.VERSION_NOT_FOUND;

@Service
@RequiredArgsConstructor
public class OfferArticleAnswerService {

    private final OfferArticleAnswerRepository answerRepository;
    private final FileService fileService;
    private final NotificationService notificationService;
    private final OfferArticleVersionService versionService;

    public void submit(Long versionId) {
        OfferArticleAnswer answer = answerRepository.findByVersionId(versionId).orElseThrow(ANSWER_NOT_FOUND.getException());
        validCompleteAnswer(answer);
        answer.setDraft(false);
        answerRepository.save(answer);
        notificationService.notifyAboutAnswer(answer);
    }

    private void validCompleteAnswer(OfferArticleAnswer answer) {
        if (answer.getDocumentId() == null && isBlank(answer.getComment())) {
            throw ANSWER_NOT_COMPLETE.getException();
        }
    }

    public OfferArticleAnswerResponse editAnswer(Long versionId, EditOfferArticleAnswerRequest request) {
        OfferArticleVersion version = versionService.findById(versionId).orElseThrow(VERSION_NOT_FOUND.getException());
        OfferArticleAnswer answer = version.getAnswer();
        if (answer == null) {
            answer = OfferArticleAnswer.builder().versionId(versionId).build();
        }
        if (request.getDocumentId() != null) {
            fileService.validateAccess(request.getDocumentId());
        }
        answer.setDocumentId(parseUuid(request.getDocumentId()));
        answer.setComment(request.getComment());
        if (!answer.isDraft()) {
            validCompleteAnswer(answer);
        }
        return convertToResponse(answerRepository.save(answer));
    }

    private OfferArticleAnswerResponse convertToResponse(OfferArticleAnswer answer) {
        return new OfferArticleAnswerResponse(answer);
    }
}
