package ru.jsms.backend.admin.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.jsms.backend.admin.dto.request.JournalRequest;
import ru.jsms.backend.admin.dto.request.SectionRequest;
import ru.jsms.backend.admin.dto.response.JournalFullResponse;
import ru.jsms.backend.admin.dto.response.JournalResponse;
import ru.jsms.backend.admin.dto.response.SectionResponse;
import ru.jsms.backend.admin.entity.Article;
import ru.jsms.backend.admin.entity.ArticleAssignment;
import ru.jsms.backend.admin.entity.Journal;
import ru.jsms.backend.admin.entity.Section;
import ru.jsms.backend.admin.repository.ArticleAssignmentRepository;
import ru.jsms.backend.admin.repository.JournalRepository;
import ru.jsms.backend.admin.repository.SectionRepository;
import ru.jsms.backend.common.dto.PageDto;
import ru.jsms.backend.common.dto.PageParam;
import ru.jsms.backend.files.service.FileService;

import javax.transaction.Transactional;
import java.util.Collections;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import static ru.jsms.backend.admin.enums.AdminArticleExceptionCode.ARTICLE_ALREADY_ASSIGNED;
import static ru.jsms.backend.admin.enums.AdminArticleExceptionCode.JOURNAL_EDIT_DENIED;
import static ru.jsms.backend.admin.enums.AdminArticleExceptionCode.JOURNAL_NOT_COMPLETE;
import static ru.jsms.backend.admin.enums.AdminArticleExceptionCode.JOURNAL_NOT_FOUND;
import static ru.jsms.backend.admin.enums.AdminArticleExceptionCode.SECTION_NOT_FOUND;

@Service
@RequiredArgsConstructor
public class JournalAdminService {

    private final JournalRepository journalRepository;
    private final SectionRepository sectionRepository;
    private final FileService fileService;
    private final ArticleService articleService;
    private final ArticleAssignmentRepository articleAssignmentRepository;

    public PageDto<JournalResponse> getAllJournals(PageParam pageParam) {
        return new PageDto<>(journalRepository.findAll(pageParam.toPageable())
                .map(this::convertToResponse));
    }

    public JournalFullResponse createJournal(JournalRequest request) {
        if (request.getFileId() != null) {
            fileService.validateAccess(request.getFileId());
        }
        Journal journal = Journal.builder()
                .volume(request.getVolume())
                .number(request.getNumber())
                .issn(request.getIssn())
                .fileId(request.getFileId())
                .build();
        return convertToFullResponse(journalRepository.save(journal));
    }

    public JournalFullResponse getJournal(Long id) {
        return convertToFullResponse(journalRepository.findById(id).orElseThrow(JOURNAL_NOT_FOUND.getException()));
    }

    public JournalFullResponse editJournal(Long id, JournalRequest request) {
        Journal journal = journalRepository.findById(id).orElseThrow();
        if (journal.getPublished()) {
            throw JOURNAL_EDIT_DENIED.getException();
        }
        if (request.getFileId() != null) {
            fileService.validateAccess(request.getFileId());
        }
        journal.setVolume(request.getVolume());
        journal.setNumber(request.getNumber());
        journal.setIssn(request.getIssn());
        journal.setFileId(request.getFileId());
        return convertToFullResponse(journalRepository.save(journal));
    }

    @Transactional
    public void deleteJournal(Long id) {
        Journal journal = journalRepository.findById(id).orElse(null);
        if (journal == null) {
            return;
        }
        if (journal.getPublished()) {
            throw JOURNAL_EDIT_DENIED.getException();
        }
        journal.getSections().forEach(this::deleteSection);
        journalRepository.delete(journal);
    }

    @Transactional
    public void publishJournal(Long id) {
        Journal journal = journalRepository.findById(id).orElseThrow(JOURNAL_NOT_FOUND.getException());
        if (!journal.isComplete()) {
            throw JOURNAL_NOT_COMPLETE.getException();
        }
        journal.getSections().stream().flatMap(s -> s.getArticleAssignments().stream())
                .map(ArticleAssignment::getArticle)
                .forEach(articleService::publish);
        journal.setPublished(true);
        journalRepository.save(journal);
    }

    @Transactional
    public SectionResponse createSection(Long journalId, SectionRequest request) {
        journalRepository.findById(journalId).orElseThrow(JOURNAL_NOT_FOUND.getException());
        Section section = sectionRepository.save(Section.builder()
                .journalId(journalId)
                .name(request.getName())
                .build());
        Set<Article> articles = articleService.findAll(request.getArticleIds());
        checkArticlesNotAssignedToOtherSection(articles, section);
        assignArticles(articles, section);
        return new SectionResponse(sectionRepository.save(section));
    }

    @Transactional
    public SectionResponse editSection(Long sectionId, SectionRequest request) {
        Section section = sectionRepository.findById(sectionId).orElseThrow(SECTION_NOT_FOUND.getException());
        if (section.getJournal().getPublished()) {
            throw JOURNAL_EDIT_DENIED.getException();
        }
        Set<Article> articles = articleService.findAll(request.getArticleIds());
        checkArticlesNotAssignedToOtherSection(articles, section);

        // удаление привязок для открепленных статей
        Set<ArticleAssignment> newArticleAssignments = articles.stream().map(Article::getArticleAssignment)
                .collect(Collectors.toSet());
        section.getArticleAssignments().stream().filter(a -> !newArticleAssignments.contains(a))
                .forEach(articleAssignmentRepository::delete);

        assignArticles(articles, section);

        section.setName(request.getName());
        return new SectionResponse(sectionRepository.save(section));
    }

    @Transactional
    public void deleteSection(Long sectionId) {
        Section section = sectionRepository.findById(sectionId).orElse(null);
        if (section == null) {
            return;
        }
        deleteSection(section);
    }

    @Transactional
    public void deleteSection(Section section) {
        if (section.getJournal().getPublished()) {
            throw JOURNAL_EDIT_DENIED.getException();
        }
        articleAssignmentRepository.deleteAll(section.getArticleAssignments());
        sectionRepository.delete(section);
    }

    private void assignArticles(Set<Article> articles, Section section) {
        articles.stream().filter(a -> a.getArticleAssignment() == null).forEach(article -> {
            ArticleAssignment articleAssignment = ArticleAssignment.builder().section(section).article(article).build();
            articleAssignmentRepository.save(articleAssignment);
            section.getArticleAssignments().add(articleAssignment);
        });
    }

    private void checkArticlesNotAssignedToOtherSection(Set<Article> articles, Section section) {
        articles.stream().map(Article::getArticleAssignment).filter(Objects::nonNull)
                .map(ArticleAssignment::getSection)
                .filter(s -> !s.equals(section)).findAny().ifPresent(a -> {
                    throw ARTICLE_ALREADY_ASSIGNED.getException();
                });
    }

    private JournalResponse convertToResponse(Journal journal) {
        return JournalResponse.builder()
                .id(journal.getId())
                .volume(journal.getVolume())
                .number(journal.getNumber())
                .issn(journal.getIssn())
                .published(journal.getPublished())
                .build();
    }

    private JournalFullResponse convertToFullResponse(Journal journal) {
        return JournalFullResponse.builder()
                .id(journal.getId())
                .volume(journal.getVolume())
                .number(journal.getNumber())
                .issn(journal.getIssn())
                .fileId(journal.getFileId())
                .published(journal.getPublished())
                .publishingDate(journal.getPublished() ? journal.getUpdated().toLocalDate() : null)
                .sections(journal.getSections() == null ? Collections.emptySet()
                        : journal.getSections().stream().map(SectionResponse::new).collect(Collectors.toSet()))
                .build();
    }
}
