package ru.jsms.backend.admin.service;

import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.jsms.backend.admin.dto.request.EditArticleRequest;
import ru.jsms.backend.admin.dto.response.ArticleFullResponse;
import ru.jsms.backend.admin.dto.response.ArticleResponse;
import ru.jsms.backend.admin.dto.response.ArticleVersionResponse;
import ru.jsms.backend.admin.dto.response.OfferArticleAnswerResponse;
import ru.jsms.backend.admin.dto.response.ReviewResponse;
import ru.jsms.backend.admin.entity.Article;
import ru.jsms.backend.admin.enums.ArticleStatus;
import ru.jsms.backend.admin.repository.ArticleRepository;
import ru.jsms.backend.common.dto.PageDto;
import ru.jsms.backend.common.dto.PageParam;
import ru.jsms.backend.user.dto.response.AuthorResponse;
import ru.jsms.backend.user.entity.OfferArticle;
import ru.jsms.backend.user.entity.OfferArticleVersion;
import ru.jsms.backend.user.repository.OfferArticleVersionRepository;

import javax.annotation.Nullable;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static ru.jsms.backend.admin.enums.AdminArticleExceptionCode.ARTICLE_NOT_ACCEPTED;
import static ru.jsms.backend.admin.enums.AdminArticleExceptionCode.ARTICLE_NOT_FOUND;

@Service
@RequiredArgsConstructor
public class ArticleService {

    private final ArticleRepository articleRepository;
    private final OfferArticleVersionRepository versionRepository;
    private final ArticleStatusUpdateService articleStatusUpdateService;
    private final ArticleStatusHistoryService articleStatusHistoryService;

    @Transactional
    public void createArticle(OfferArticle offerArticle) {
        var article = Article.builder().offerArticle(offerArticle).build();
        articleStatusUpdateService.updateStatus(article, ArticleStatus.NEW);
        articleRepository.save(article);
    }

    public PageDto<ArticleResponse> getAllArticles(PageParam pageParam, @Nullable ArticleStatus[] statuses) {
        Page<Article> articles;
        if (ArrayUtils.isEmpty(statuses)) {
            articles = articleRepository.findAll(pageParam.toPageable());
        } else {
            articles = articleRepository.findByStatuses(statuses, pageParam.toPageable());
        }
        return new PageDto<>(articles.map(this::convertToResponse));
    }

    public ArticleFullResponse getArticle(Long articleId) {
        return convertToFullResponse(articleRepository.findById(articleId).orElseThrow(ARTICLE_NOT_FOUND.getException()));
    }

    @Transactional
    public ArticleFullResponse editArticle(Long articleId, EditArticleRequest request) {
        Article article = articleRepository.findById(articleId).orElseThrow(ARTICLE_NOT_FOUND.getException());
        articleStatusUpdateService.updateStatus(article, request.getStatus());
        article.setComment(request.getComment());
        return convertToFullResponse(articleRepository.save(article));
    }

    public PageDto<ArticleVersionResponse> getArticleVersions(Long articleId, PageParam pageParam) {
        Article article = articleRepository.findById(articleId).orElseThrow(ARTICLE_NOT_FOUND.getException());
        return new PageDto<>(
                versionRepository.findByOfferArticleIdAndDraftIsFalse(
                        article.getOfferArticle().getId(), pageParam.toPageable()).map(this::convertToResponse)
        );
    }

    public Set<Article> findAll(Set<Long> articleIds) {
        return articleRepository.findAll(articleIds);
    }

    @Transactional
    public void publish(Article article) {
        if (article.getStatus() != ArticleStatus.ACCEPTED) {
            throw ARTICLE_NOT_ACCEPTED.getException();
        }
        articleStatusUpdateService.updateStatus(article, ArticleStatus.PUBLISHED);
    }

    private ArticleVersionResponse convertToResponse(OfferArticleVersion version) {
        return ArticleVersionResponse.builder()
                .id(version.getId())
                .articleArchiveId(version.getArticleArchiveId())
                .documentsArchiveId(version.getDocumentsArchiveId())
                .comment(version.getComment())
                .answer(Optional.ofNullable(version.getAnswer()).map(OfferArticleAnswerResponse::new).orElse(null))
                .review(Optional.ofNullable(version.getReview()).map(ReviewResponse::new).orElse(null))
                .created(version.getCreated())
                .build();
    }

    private ArticleResponse convertToResponse(Article article) {
        return ArticleResponse.builder()
                .id(article.getId())
                .name(article.getOfferArticle().getName())
                .status(article.getStatus())
                .comment(article.getComment())
                .created(article.getCreated())
                .build();
    }

    private ArticleFullResponse convertToFullResponse(Article article) {
        return ArticleFullResponse.builder()
                .id(article.getId())
                .name(article.getOfferArticle().getName())
                .udk(article.getOfferArticle().getUdk())
                .annotation(article.getOfferArticle().getAnnotation())
                .status(article.getStatus())
                .comment(article.getComment())
                .created(article.getCreated())
                .authors(article.getOfferArticle().getAuthors().stream().map(AuthorResponse::new)
                        .collect(Collectors.toSet()))
                .statusHistory(articleStatusHistoryService.getArticleHistory(article.getId()))
                .build();
    }
}
