package ru.jsms.backend.admin.service;

import ru.jsms.backend.admin.enums.ArticleStatus;
import ru.jsms.backend.user.enums.OfferArticleStatus;

public class ArticleStatusMapper {

    public static OfferArticleStatus map(ArticleStatus status) {
        return switch (status) {
            case NEW, CONSIDERATION, REVIEW -> OfferArticleStatus.CONSIDERATION;
            case REVISION -> OfferArticleStatus.REVISION;
            case REJECTED -> OfferArticleStatus.REJECTED;
            case ACCEPTED -> OfferArticleStatus.ACCEPTED;
            case PUBLISHED -> OfferArticleStatus.PUBLISHED;
        };
    }
}
