package ru.jsms.backend.admin.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.jsms.backend.admin.dto.response.ArticleStatusHistoryDto;
import ru.jsms.backend.admin.repository.ArticleRepository;
import ru.jsms.backend.admin.repository.ArticleStatusHistoryRepository;
import ru.jsms.backend.user.dto.response.OfferArticleStatusHistoryDto;
import ru.jsms.backend.user.enums.OfferArticleStatus;

import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Stream;

@Service
@RequiredArgsConstructor
public class ArticleStatusHistoryService {

    private final ArticleStatusHistoryRepository articleStatusHistoryRepository;
    private final ArticleRepository articleRepository;

    public List<OfferArticleStatusHistoryDto> getOfferHistory(Long offerArticleId) {
        AtomicReference<OfferArticleStatus> prevStatus = new AtomicReference<>();
        return Stream.ofNullable(articleRepository.findByOfferArticleId(offerArticleId))
                .flatMap(a -> articleStatusHistoryRepository.findByArticleIdOrderByUpdated(a.getId()).stream())
                .map(s -> new OfferArticleStatusHistoryDto(
                        ArticleStatusMapper.map(s.getStatus()),
                        s.getUpdated())
                )
                .filter(r -> {
                    boolean differentFromPrev = r.getStatus() != prevStatus.get();
                    prevStatus.set(r.getStatus());
                    return differentFromPrev;
                })
                .toList();
    }

    public List<ArticleStatusHistoryDto> getArticleHistory(Long articleId) {
        return articleStatusHistoryRepository.findByArticleIdOrderByUpdated(articleId).stream()
                .map(ArticleStatusHistoryDto::new).toList();
    }
}
