package ru.jsms.backend.admin.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.jsms.backend.admin.entity.Article;
import ru.jsms.backend.admin.entity.ArticleStatusHistory;
import ru.jsms.backend.admin.enums.ArticleStatus;
import ru.jsms.backend.admin.repository.ArticleRepository;
import ru.jsms.backend.admin.repository.ArticleStatusHistoryRepository;
import ru.jsms.backend.profile.service.NotificationService;
import ru.jsms.backend.user.repository.OfferArticleRepository;

import javax.transaction.Transactional;

@Service
@RequiredArgsConstructor
public class ArticleStatusUpdateService {

    private final ArticleRepository articleRepository;
    private final OfferArticleRepository offerArticleRepository;
    private final ArticleStatusHistoryRepository articleStatusHistoryRepository;
    private final NotificationService notificationService;

    @Transactional
    public void updateStatus(Article article, ArticleStatus status) {
        if (status == null || status == article.getStatus()) {
            return;
        }
        article.setStatus(status);
        articleRepository.save(article);
        var offerArticle = article.getOfferArticle();
        var newOfferArticleStatus = ArticleStatusMapper.map(status);
        if (offerArticle.getStatus() != newOfferArticleStatus) {
            offerArticle.setStatus(newOfferArticleStatus);
            notificationService.notifyAboutOfferArticleStatusChanged(offerArticle);
        }
        offerArticleRepository.save(offerArticle);
        articleStatusHistoryRepository.save(ArticleStatusHistory.builder().articleId(article.getId())
                .status(status).build());
    }

}
