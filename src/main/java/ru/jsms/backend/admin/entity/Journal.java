package ru.jsms.backend.admin.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import ru.jsms.backend.common.entity.BaseEntity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder(toBuilder = true)
@Entity
public class Journal extends BaseEntity<Long> {

    private Integer volume;

    private Integer number;

    private String issn;

    private UUID fileId;

    @OneToMany(mappedBy = "journal", cascade = CascadeType.ALL)
    @Builder.Default
    private Set<Section> sections = new HashSet<>();

    public Set<Section> getSections() {
        return sections.stream().filter(s -> !s.isDeleted()).collect(Collectors.toSet());
    }

    @Builder.Default
    private Boolean published = false;

    public boolean isComplete() {
        if (volume == null || number == null || issn == null || fileId == null || sections.isEmpty())
            return false;
        for (Section section : sections.stream().filter(s -> !s.isDeleted()).collect(Collectors.toSet())) {
            if (!section.isComplete())
                return false;
        }
        return true;
    }
}
