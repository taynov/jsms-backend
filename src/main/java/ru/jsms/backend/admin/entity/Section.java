package ru.jsms.backend.admin.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import ru.jsms.backend.common.entity.BaseEntity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder(toBuilder = true)
@Entity
public class Section extends BaseEntity<Long> {

    private String name;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "section_id")
    @Builder.Default
    private Set<ArticleAssignment> articleAssignments = new HashSet<>();

    public Set<ArticleAssignment> getArticleAssignments() {
        return articleAssignments.stream().filter(s -> !s.isDeleted()).collect(Collectors.toSet());
    }

    @ManyToOne
    @JoinColumn(name = "journal_id", insertable = false, updatable = false)
    private Journal journal;
    @Column(name = "journal_id")
    private Long journalId;

    public boolean isComplete() {
        return name != null && journalId != null && !articleAssignments.isEmpty();
    }
}