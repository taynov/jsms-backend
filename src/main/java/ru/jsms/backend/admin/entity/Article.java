package ru.jsms.backend.admin.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import ru.jsms.backend.admin.enums.ArticleStatus;
import ru.jsms.backend.common.entity.BaseEntity;
import ru.jsms.backend.user.entity.OfferArticle;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import java.util.Set;

@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder(toBuilder = true)
@Entity
public class Article extends BaseEntity<Long> {

    @Getter
    @OneToOne
    @JoinColumn(name = "offer_article_id")
    private OfferArticle offerArticle;

    @Getter
    @Enumerated(EnumType.STRING)
    private ArticleStatus status;

    @Getter
    private String comment;

    @OneToMany(mappedBy = "article", cascade = CascadeType.ALL)
    private Set<ArticleAssignment> articleAssignments;

    public ArticleAssignment getArticleAssignment() {
        return articleAssignments.stream().filter(a -> !a.isDeleted()).findAny().orElse(null);
    }
}
