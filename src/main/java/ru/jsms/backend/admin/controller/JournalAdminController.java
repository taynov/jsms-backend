package ru.jsms.backend.admin.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.jsms.backend.admin.dto.request.JournalRequest;
import ru.jsms.backend.admin.dto.request.SectionRequest;
import ru.jsms.backend.admin.dto.response.JournalFullResponse;
import ru.jsms.backend.admin.dto.response.JournalResponse;
import ru.jsms.backend.admin.dto.response.SectionResponse;
import ru.jsms.backend.admin.service.JournalAdminService;
import ru.jsms.backend.common.dto.PageDto;
import ru.jsms.backend.common.dto.PageParam;

@RestController
@RequestMapping("/api/v1/admin/journals")
@RequiredArgsConstructor
@PreAuthorize("hasAuthority('ADMIN')")
public class JournalAdminController {

    private final JournalAdminService journalService;

    @GetMapping
    public ResponseEntity<PageDto<JournalResponse>> getAllJournals(PageParam pageParam) {
        return ResponseEntity.ok(journalService.getAllJournals(pageParam));
    }

    @PostMapping
    public ResponseEntity<JournalFullResponse> createJournal(@RequestBody JournalRequest request) {
        return ResponseEntity.ok(journalService.createJournal(request));
    }

    @GetMapping("/{id}")
    public ResponseEntity<JournalFullResponse> getJournal(@PathVariable Long id) {
        return ResponseEntity.ok(journalService.getJournal(id));
    }

    @PutMapping("/{id}")
    public ResponseEntity<JournalFullResponse> editJournal(@PathVariable Long id, @RequestBody JournalRequest request) {
        return ResponseEntity.ok(journalService.editJournal(id, request));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteJournal(@PathVariable Long id) {
        journalService.deleteJournal(id);
        return ResponseEntity.ok().build();
    }

    @PostMapping("/{id}/publish")
    public ResponseEntity<Void> publishJournal(@PathVariable Long id) {
        journalService.publishJournal(id);
        return ResponseEntity.ok().build();
    }

    @PostMapping("/{journalId}/sections")
    public ResponseEntity<SectionResponse> createSection(@PathVariable Long journalId,
                                                         @RequestBody SectionRequest request) {
        return ResponseEntity.ok(journalService.createSection(journalId, request));
    }

    @PutMapping("/sections/{sectionId}")
    public ResponseEntity<SectionResponse> editSection(@PathVariable Long sectionId,
                                                           @RequestBody SectionRequest request) {
        return ResponseEntity.ok(journalService.editSection(sectionId, request));
    }

    @DeleteMapping("/sections/{sectionId}")
    public ResponseEntity<SectionResponse> deleteSection(@PathVariable Long sectionId) {
        journalService.deleteSection(sectionId);
        return ResponseEntity.ok().build();
    }
}
