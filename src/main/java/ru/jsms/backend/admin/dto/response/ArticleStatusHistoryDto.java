package ru.jsms.backend.admin.dto.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import ru.jsms.backend.admin.entity.ArticleStatusHistory;
import ru.jsms.backend.admin.enums.ArticleStatus;

import java.time.LocalDateTime;

@Data
@Builder
@AllArgsConstructor
public class ArticleStatusHistoryDto {

    private ArticleStatus status;
    private LocalDateTime updated;

    public ArticleStatusHistoryDto(ArticleStatusHistory articleStatusHistory) {
        this.status = articleStatusHistory.getStatus();
        this.updated = articleStatusHistory.getUpdated();
    }
}
