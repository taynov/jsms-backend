package ru.jsms.backend.admin.dto.response;

import lombok.Builder;
import lombok.Data;
import ru.jsms.backend.admin.enums.ArticleStatus;

import java.time.LocalDateTime;

@Data
@Builder
public class ArticleResponse {
    private Long id;
    private String name;
    private ArticleStatus status;
    private String comment;
    private LocalDateTime created;
}
