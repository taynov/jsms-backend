package ru.jsms.backend.admin.dto.response;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDate;
import java.util.Set;
import java.util.UUID;

@Data
@Builder
public class JournalFullResponse {
    private Long id;
    private Integer volume;
    private Integer number;
    private String issn;
    private UUID fileId;
    private Boolean published;
    private LocalDate publishingDate;
    private Set<SectionResponse> sections;
}
