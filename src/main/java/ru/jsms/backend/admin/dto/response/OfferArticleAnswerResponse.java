package ru.jsms.backend.admin.dto.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import ru.jsms.backend.admin.entity.OfferArticleAnswer;

import java.time.LocalDateTime;
import java.util.UUID;

@Data
@AllArgsConstructor
@Builder
public class OfferArticleAnswerResponse {
    private UUID documentId;
    private String comment;
    private boolean isDraft;
    private LocalDateTime created;
    private LocalDateTime updated;

    public OfferArticleAnswerResponse(OfferArticleAnswer answer) {
        this.documentId = answer.getDocumentId();
        this.comment = answer.getComment();
        this.isDraft = answer.isDraft();
        this.created = answer.getCreated();
        this.updated = answer.getUpdated();
    }
}
