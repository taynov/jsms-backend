package ru.jsms.backend.admin.dto.response;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class JournalResponse {
    private Long id;
    private Integer volume;
    private Integer number;
    private String issn;
    private Boolean published;
}
