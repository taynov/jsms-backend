package ru.jsms.backend.admin.dto.response;

import lombok.Builder;
import lombok.Data;
import ru.jsms.backend.admin.enums.ArticleStatus;
import ru.jsms.backend.user.dto.response.AuthorResponse;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

@Data
@Builder
public class ArticleFullResponse {
    private Long id;
    private String name;
    private String udk;
    private String annotation;
    private ArticleStatus status;
    private String comment;
    private LocalDateTime created;
    private Set<AuthorResponse> authors;
    private List<ArticleStatusHistoryDto> statusHistory;
}
