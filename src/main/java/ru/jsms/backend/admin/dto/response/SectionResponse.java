package ru.jsms.backend.admin.dto.response;

import lombok.Data;
import ru.jsms.backend.admin.entity.ArticleAssignment;
import ru.jsms.backend.admin.entity.Section;

import java.util.Set;
import java.util.stream.Collectors;

@Data
public class SectionResponse {
    private Long id;
    private String name;
    private Set<ArticleSectionResponse> articles;

    public SectionResponse(Section section) {
        this.id = section.getId();
        this.name = section.getName();
        this.articles = section.getArticleAssignments().stream().map(ArticleAssignment::getArticle)
                .map(ArticleSectionResponse::new).collect(Collectors.toSet());
    }
}
