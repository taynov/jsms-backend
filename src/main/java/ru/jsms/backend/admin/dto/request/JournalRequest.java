package ru.jsms.backend.admin.dto.request;

import lombok.Data;

import java.util.UUID;

@Data
public class JournalRequest {
    private Integer volume;
    private Integer number;
    private String issn;
    private UUID fileId;
}
