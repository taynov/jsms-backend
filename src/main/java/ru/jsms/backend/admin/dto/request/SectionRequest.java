package ru.jsms.backend.admin.dto.request;

import lombok.Data;

import java.util.Set;

@Data
public class SectionRequest {
    private String name;
    private Set<Long> articleIds;
}
