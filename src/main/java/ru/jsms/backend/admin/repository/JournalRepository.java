package ru.jsms.backend.admin.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import ru.jsms.backend.admin.entity.Journal;
import ru.jsms.backend.common.repository.BaseRepository;

public interface JournalRepository extends BaseRepository<Journal, Long> {
    @Query("select j from Journal j where j.published = true and j.deleted = false")
    Page<Journal> findPublished(Pageable pageable);
}
