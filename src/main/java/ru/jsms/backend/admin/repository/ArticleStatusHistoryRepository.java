package ru.jsms.backend.admin.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.jsms.backend.admin.entity.ArticleStatusHistory;

import java.util.List;

@Repository
public interface ArticleStatusHistoryRepository extends JpaRepository<ArticleStatusHistory, Long> {
    List<ArticleStatusHistory> findByArticleIdOrderByUpdated(Long articleId);
}
