package ru.jsms.backend.admin.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.jsms.backend.admin.entity.Article;
import ru.jsms.backend.admin.enums.ArticleStatus;
import ru.jsms.backend.common.repository.BaseRepository;

@Repository
public interface ArticleRepository extends BaseRepository<Article, Long> {
    @Query("select a from Article a where a.status in :statuses and a.deleted = false")
    Page<Article> findByStatuses(ArticleStatus[] statuses, Pageable pageable);

    @Query("select a from Article a where a.offerArticle.id = :offerArticleId and a.deleted = false")
    Article findByOfferArticleId(Long offerArticleId);
}
