package ru.jsms.backend.admin.repository;

import ru.jsms.backend.admin.entity.ArticleAssignment;
import ru.jsms.backend.common.repository.BaseRepository;

public interface ArticleAssignmentRepository extends BaseRepository<ArticleAssignment, Long> {
}
