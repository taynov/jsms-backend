package ru.jsms.backend.admin.repository;

import ru.jsms.backend.admin.entity.Section;
import ru.jsms.backend.common.repository.BaseRepository;

public interface SectionRepository extends BaseRepository<Section, Long> {
}
