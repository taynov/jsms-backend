package ru.jsms.backend.profile.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.jsms.backend.common.dto.HeadersDto;
import ru.jsms.backend.profile.service.EmailConfirmationService;

@RestController
@RequestMapping("/api/v1/email")
@RequiredArgsConstructor
public class EmailConfirmationController {

    private final EmailConfirmationService emailConfirmationService;

    @PostMapping("/sendCode")
    public void sendEmailConfirmationCode() {
        emailConfirmationService.sendCode();
    }

    @GetMapping("/confirm")
    public void confirmEmail(@RequestParam String code) {
        emailConfirmationService.confirm(code);
    }
}
