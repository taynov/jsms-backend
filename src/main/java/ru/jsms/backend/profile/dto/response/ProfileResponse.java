package ru.jsms.backend.profile.dto.response;

import lombok.Builder;
import lombok.Data;
import ru.jsms.backend.common.dto.Role;

import java.util.List;

@Data
@Builder
public class ProfileResponse {
    private Long id;
    private String firstName;
    private String patronymic;
    private String secondName;
    private String email;
    private Boolean isConfirmed;
    private List<Role> canBeRoles;
}
