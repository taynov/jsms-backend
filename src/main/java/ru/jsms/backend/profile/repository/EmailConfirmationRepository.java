package ru.jsms.backend.profile.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.jsms.backend.profile.entity.EmailConfirmation;

import java.util.Optional;

public interface EmailConfirmationRepository extends JpaRepository<EmailConfirmation, String> {
    Optional<EmailConfirmation> findByEmail(String email);
}