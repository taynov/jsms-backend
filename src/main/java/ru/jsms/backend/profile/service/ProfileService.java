package ru.jsms.backend.profile.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.jsms.backend.common.dto.HeadersDto;
import ru.jsms.backend.profile.dto.response.ProfileResponse;
import ru.jsms.backend.profile.entity.EmailConfirmation;
import ru.jsms.backend.profile.entity.UserData;
import ru.jsms.backend.profile.repository.EmailConfirmationRepository;
import ru.jsms.backend.profile.repository.UserDataRepository;

import static ru.jsms.backend.profile.enums.AuthExceptionCode.ACCOUNT_NOT_FOUND;

@Service
@RequiredArgsConstructor
public class ProfileService {
    private final HeadersDto headersDto;

    private final UserDataRepository userDataRepository;
    private final EmailConfirmationRepository emailConfirmationRepository;

    public ProfileResponse getProfile() {
        Long userId = headersDto.getUserId();
        UserData userData = userDataRepository.findById(userId).orElseThrow(ACCOUNT_NOT_FOUND.getException());

        Boolean isConfirmed = emailConfirmationRepository.findById(userData.getEmail())
                .map(EmailConfirmation::isConfirmed).orElse(false);

        return ProfileResponse.builder()
                .id(userId)
                .firstName(userData.getFirstName())
                .patronymic(userData.getPatronymic())
                .secondName(userData.getSecondName())
                .email(userData.getEmail())
                .isConfirmed(isConfirmed)
                .canBeRoles(headersDto.getUser().getRoles().stream().filter(
                        it -> headersDto.getCurrentRole() != null && it != headersDto.getCurrentRole()
                ).toList())
                .build();
    }
}
