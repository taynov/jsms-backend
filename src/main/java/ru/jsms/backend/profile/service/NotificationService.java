package ru.jsms.backend.profile.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import ru.jsms.backend.admin.entity.OfferArticleAnswer;
import ru.jsms.backend.profile.entity.EmailConfirmation;
import ru.jsms.backend.user.entity.OfferArticle;

@Service
@Slf4j
@RequiredArgsConstructor
public class NotificationService {

    @Value("${spring.mail.username}")
    private String sender;
    private final JavaMailSender mailSender;
    private final UserService userService;

    @Async
    public void notifyAboutAnswer(OfferArticleAnswer answer) {
        SimpleMailMessage message = buildOfferArticleAnswerMessage(answer);
        log.info("На почту {} отправлено письмо: {}", message.getTo(), message.getText());
        mailSender.send(message);
    }

    @Async
    public void notifyAboutEmailConfirmation(EmailConfirmation emailConfirmation) {
        SimpleMailMessage message = buildEmailConfirmationMessage(emailConfirmation);
        log.info("На почту {} отправлено письмо: {}", message.getTo(), message.getText());
        mailSender.send(message);
    }

    @Async
    public void notifyAboutOfferArticleStatusChanged(OfferArticle offerArticle) {
        SimpleMailMessage message = buildOfferArticleStatusChangedMessage(offerArticle);
        log.info("На почту {} отправлено письмо: {}", message.getTo(), message.getText());
        mailSender.send(message);
    }

    private SimpleMailMessage buildOfferArticleStatusChangedMessage(OfferArticle offerArticle) {
        long ownerId = offerArticle.getOwnerId();
        String email = userService.getUserDataById(ownerId).getEmail();
        SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
        simpleMailMessage.setFrom(sender);
        simpleMailMessage.setTo(email);
        simpleMailMessage.setSubject("Новый статус заявки");
        simpleMailMessage.setText("Редактор изменил статус вашей заявки");
        return simpleMailMessage;
    }

    private SimpleMailMessage buildOfferArticleAnswerMessage(OfferArticleAnswer answer) {
        long ownerId = answer.getOfferArticleVersion().getOwnerId();
        String email = userService.getUserDataById(ownerId).getEmail();

        SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
        simpleMailMessage.setFrom(sender);
        simpleMailMessage.setTo(email);
        simpleMailMessage.setSubject("Ответ редактора");
        simpleMailMessage.setText("Редактор отправил ответ на вашу заявку");
        return simpleMailMessage;
    }

    private SimpleMailMessage buildEmailConfirmationMessage(EmailConfirmation emailConfirmation) {
        SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
        simpleMailMessage.setFrom(sender);
        simpleMailMessage.setTo(emailConfirmation.getEmail());
        simpleMailMessage.setSubject("Подтверждение почты");
        simpleMailMessage.setText(
                "Код подтверждения: " + emailConfirmation.getCode() + "\n" +
                        "Срок действия: " + emailConfirmation.getExpiryDate()
        );
        return simpleMailMessage;
    }
}
