package ru.jsms.backend.profile.service;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import ru.jsms.backend.common.dto.HeadersDto;
import ru.jsms.backend.profile.entity.EmailConfirmation;
import ru.jsms.backend.profile.repository.EmailConfirmationRepository;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.random.RandomGenerator;

import static ru.jsms.backend.profile.enums.AuthExceptionCode.EMAIL_ALREADY_CONFIRMED;
import static ru.jsms.backend.profile.enums.AuthExceptionCode.EMAIL_CODE_INVALID;
import static ru.jsms.backend.profile.enums.AuthExceptionCode.EMAIL_CODE_NOT_SENT;

@Service
@RequiredArgsConstructor
public class EmailConfirmationService {

    @Value("${email.verification.ttl}")
    private int ttl;

    private final EmailConfirmationRepository emailConfirmationRepository;
    private final NotificationService notificationService;
    private final RandomGenerator random;
    private final HeadersDto headersDto;

    public void sendCode() {
        String userEmail = headersDto.getUser().getUserData().getEmail();
        emailConfirmationRepository.findById(userEmail)
                .filter(EmailConfirmation::isConfirmed)
                .ifPresent(s -> {
                    throw EMAIL_ALREADY_CONFIRMED.getException();
                });
        EmailConfirmation emailConfirmation = EmailConfirmation.builder()
                .email(userEmail)
                .code(getCode())
                .expiryDate(Instant.now().plus(ttl, ChronoUnit.MINUTES))
                .confirmed(false)
                .build();
        emailConfirmationRepository.save(emailConfirmation);
        notificationService.notifyAboutEmailConfirmation(emailConfirmation);
    }

    public void confirm(String code) {
        String email = headersDto.getUser().getUserData().getEmail();
        EmailConfirmation emailConfirmation = emailConfirmationRepository.findByEmail(email)
                .orElseThrow(EMAIL_CODE_NOT_SENT.getException());
        if (!emailConfirmation.getCode().equals(code) || emailConfirmation.getExpiryDate().isBefore(Instant.now())) {
            throw EMAIL_CODE_INVALID.getException();
        }
        emailConfirmation.setConfirmed(true);
        emailConfirmationRepository.save(emailConfirmation);
    }

    private String getCode() {
        return String.format("%04d", random.nextInt(10000));
    }
}
