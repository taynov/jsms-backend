package ru.jsms.backend.profile.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum ClaimsCode {
    USER_ID("userId"),
    ROLE("role");

    private final String name;
}
