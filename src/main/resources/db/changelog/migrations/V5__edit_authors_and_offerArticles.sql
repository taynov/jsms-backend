ALTER TABLE author
    ADD post VARCHAR(50);

ALTER TABLE author
    ADD orcid VARCHAR(19);

ALTER TABLE author
    ADD organization_name VARCHAR(255);

ALTER TABLE author
    ADD organization_address VARCHAR(255);

ALTER TABLE author
    ADD academic_degree VARCHAR(50);

ALTER TABLE offer_article
    ADD udk VARCHAR(20);

ALTER TABLE offer_article
    ADD annotation TEXT;