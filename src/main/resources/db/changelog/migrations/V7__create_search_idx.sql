create index author_search_idx on author
    using gin(
              (setweight(to_tsvector('simple', second_name), 'A') ||
               setweight(to_tsvector('simple', first_name), 'B') ||
               setweight(to_tsvector('simple', coalesce(patronymic, '')), 'C'))
        );