INSERT INTO users (password, roles)
VALUES ('$2a$10$KUdsO6JTCeloEBN/WpEd0.vygJ5pQvGPIxl3/6IGKQgJJBHLRDhyy', 'USER'); -- Qwerty123!
INSERT INTO user_data (user_id, email, first_name, second_name)
VALUES (lastval(), 'user@mail.ru', 'User', 'Userov');

INSERT INTO users (password, roles)
VALUES ('$2a$10$KUdsO6JTCeloEBN/WpEd0.vygJ5pQvGPIxl3/6IGKQgJJBHLRDhyy', 'ADMIN'); -- Qwerty123!
INSERT INTO user_data (user_id, email, first_name, second_name)
VALUES (lastval(), 'admin@mail.ru', 'Admin', 'Adminov');