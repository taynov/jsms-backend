FROM openjdk:17

COPY target/*.jar jsms-server.jar

ENTRYPOINT ["java","-jar","/jsms-server.jar"]